// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

const pairs = (obj) => {
    let arr = [];
    for (let key in obj) {
        let pair = [];
        pair.push(key, obj[key]);
        arr.push(pair);
    }
    return arr;
}

module.exports = pairs;