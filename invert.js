// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

const invert = (obj) => {
    newObj = {};
    for (let key in obj) {
        newObj[obj[key]] = key;
    }
    return newObj;
};


module.exports = invert;