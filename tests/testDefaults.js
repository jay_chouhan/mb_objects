const defaults = require('../defaults');

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

const defaultProps = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    alias: 'Batman',
    bestFriend: 'Joker',
    enemy: 'Joker'
};

result = defaults(testObject, defaultProps);

console.log(result);
/*
{ name: 'Bruce Wayne',
  age: 36,
  location: 'Gotham',
  alias: 'Batman',
  bestFriend: 'Joker',
  enemy: 'Joker' }

*/