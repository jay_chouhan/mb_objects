const keys = require('../keys');

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

result = keys(testObject);

console.log(result); // [ 'name', 'age', 'location' ]