const invert = require('../invert')

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

result = invert(testObject);

//test case: keys and values should be inverted

console.log(result); //{ '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' }