const mapObjects = require('../mapObject');

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

const cb = (...arg) => {
    return 'I am Batman'
}

result = mapObjects(testObject, cb);

// test case: every property will have the value 'I am Batman'

console.log(result)
/*{ name: 'I am Batman',
age: 'I am Batman',
location: 'I am Batman' }
*/